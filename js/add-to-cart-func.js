
function product_pop(idPost) {
    "use strict";
    jQuery.ajax({
        url : popfunctions.ajax_url,
        type : 'post',
        data : {
            action : 'post_popfunctions',
            post_id : idPost
        },
        beforeSend: function () {
            jQuery('.overlay-thechoice').removeClass('overlay-hidden');
            jQuery('.overlay-thechoice').addClass('overlay-show');
            jQuery('.overlay-thechoice').html('<div class="loader-wrapper">' + '<div class="loader-tab"></div>' + '</div>');
        },
        success: function (resp) {
            jQuery('.overlay-thechoice').html(resp);
        }
    });
};