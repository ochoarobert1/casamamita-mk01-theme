/*
 * Bones Scripts File
 * Author: Eddie Machado
 *
 * This file should contain any js scripts you want to add to the site.
 * Instead of calling it in the header or throwing it inside wp_head()
 * this file will be called automatically in the footer so as not to
 * slow the page load.
 *
 * There are a lot of example functions and tools in here. If you don't
 * need any of it, just remove it. They are meant to be helpers and are
 * not required. It's your world baby, you can do whatever you want.
 */


/*
 * Get Viewport Dimensions
 * returns object with viewport dimensions to match css in width and height properties
 * ( source: http://andylangton.co.uk/blog/development/get-viewport-size-width-and-height-javascript )
 */
function updateViewportDimensions() {
    "use strict";
    var w = window,
        d = document,
        e = d.documentElement,
        g = d.getElementsByTagName('body')[0],
        x = w.innerWidth || e.clientWidth || g.clientWidth,
        y = w.innerHeight || e.clientHeight || g.clientHeight;
    return {
        width: x,
        height: y
    };
}
// setting the viewport width
var viewport = updateViewportDimensions();


/*
 * Throttle Resize-triggered Events
 * Wrap your actions in this function to throttle the frequency of firing them off, for better performance, esp. on mobile.
 * ( source: http://stackoverflow.com/questions/2854407/javascript-jquery-window-resize-how-to-fire-after-the-resize-is-completed )
 */
var waitForFinalEvent = (function () {
    "use strict";
    var timers = {};
    return function (callback, ms, uniqueId) {
        if (!uniqueId) {
            uniqueId = "Don't call this twice without a uniqueId";
        }
        if (timers[uniqueId]) {
            clearTimeout(timers[uniqueId]);
        }
        timers[uniqueId] = setTimeout(callback, ms);
    };
})();

// how long to wait before deciding the resize has stopped, in ms. Around 50-100 should work ok.
var timeToWaitForLast = 100;


/*
 * Here's an example so you can see how we're using the above function
 *
 * This is commented out so it won't work, but you can copy it and
 * remove the comments.
 *
 *
 *
 * If we want to only do it on a certain page, we can setup checks so we do it
 * as efficient as possible.
 *
 * if( typeof is_home === "undefined" ) var is_home = $('body').hasClass('home');
 *
 * This once checks to see if you're on the home page based on the body class
 * We can then use that check to perform actions on the home page only
 *
 * When the window is resized, we perform this function
 * $(window).resize(function () {
 *
 *    // if we're on the home page, we wait the set amount (in function above) then fire the function
 *    if( is_home ) { waitForFinalEvent( function() {
 *
 *    // update the viewport, in case the window size has changed
 *    viewport = updateViewportDimensions();
 *
 *      // if we're above or equal to 768 fire this off
 *      if( viewport.width >= 768 ) {
 *        console.log('On home page and window sized to 768 width or more.');
 *      } else {
 *        // otherwise, let's do this instead
 *        console.log('Not on home page, or window sized to less than 768.');
 *      }
 *
 *    }, timeToWaitForLast, "your-function-identifier-string"); }
 * });
 *
 * Pretty cool huh? You can create functions like this to conditionally load
 * content and other stuff dependent on the viewport.
 * Remember that mobile devices and javascript aren't the best of friends.
 * Keep it light and always make sure the larger viewports are doing the heavy lifting.
 *
 */

/*
 * We're going to swap out the gravatars.
 * In the functions.php file, you can see we're not loading the gravatar
 * images on mobile to save bandwidth. Once we hit an acceptable viewport
 * then we can swap out those images since they are located in a data attribute.
 */
function loadGravatars() {
    "use strict";
    // set the viewport using the function above
    viewport = updateViewportDimensions();
    // if the viewport is tablet or larger, we load in the gravatars
    if (viewport.width >= 768) {
        jQuery('.comment img[data-gravatar]').each(function () {
            jQuery(this).attr('src', jQuery(this).attr('data-gravatar'));
        });
    }
} // end function


/*
 * Put all your regular jQuery in here.
 */
jQuery(document).ready(function ($) {
    "use strict";
    jQuery("#sticker").sticky({
        topSpacing: 0
    });
    jQuery("#sticker-mobile").sticky({
        topSpacing: 0
    });
    jQuery(".home-product-carousel").owlCarousel({
        items: 4,
        loop: true,
        margin: 50,
        nav: true,
        dots: false,
        navText: ['<i class="fa fa-chevron-left" aria-hidden="true"></i>', '<i class="fa fa-chevron-right" aria-hidden="true"></i>'],
        responsive: {
            0: {
                items: 2
            },
            370: {
                items: 2
            },
            600: {
                items: 3
            },
            1000: {
                items: 4
            }
        }
    });
    jQuery(".home-brands-carousel").owlCarousel({
        items: 5,
        loop: true,
        margin: 10,
        nav: true,
        autoplay: true,
        dots: false,
        navText: ['<i class="fa fa-chevron-left" aria-hidden="true"></i>', '<i class="fa fa-chevron-right" aria-hidden="true"></i>'],
        responsive: {
            0: {
                items: 1
            },
            500: {
                items: 2
            },
            600: {
                items: 3
            },
            1000: {
                items: 5
            }
        }
    });

    jQuery(".dropdown-custom-child-slider").owlCarousel({
        items: 4,
        loop: false,
        margin: 10,
        nav: true,
        autoplay: false,
        dots: false,
        navText: ['<i class="fa fa-chevron-left" aria-hidden="true"></i>', '<i class="fa fa-chevron-right" aria-hidden="true"></i>'],
        responsive: {
            0: {
                items: 1
            },
            600: {
                items: 3
            },
            1000: {
                items: 4
            }
        }
    });

    /*
     * Let's fire off the gravatar function
     * You can remove this if you don't need it
     */
    loadGravatars();


}); /* end of as page load scripts */

jQuery('.custom-cart-opener').click(function () {
    "use strict";
    if (jQuery('.custom-dropdown-menu').hasClass('custom-dropdown-show')) {
        jQuery('.custom-dropdown-menu').removeClass('custom-dropdown-show');
        jQuery('.custom-dropdown-menu').addClass('custom-dropdown-hide');
    } else {
        jQuery('.custom-dropdown-menu').removeClass('custom-dropdown-hide');
        jQuery('.custom-dropdown-menu').addClass('custom-dropdown-show');
    }
});

function add_cart(post_id) {
    "use strict";
    jQuery.ajax({
        url: added_function.ajax_url,
        type: 'post',
        data: {
            action: 'post_added_function',
            post_id: post_id
        },
        beforeSend: function () {
            jQuery("#add_cart_" + post_id).addClass('home-product-item-loading');
        },
        success: function (response) {
            jQuery("#add_cart_" + post_id).addClass('home-product-item-added');
            jQuery('.the-notices').removeClass('the-notices-hidden');
            setTimeout(function () {
                jQuery('.the-notices').addClass('the-notices-hidden');
            }, 3000);
        }
    });
}

function remove_cart(post_id, post_data_id) {
    "use strict";
    jQuery.ajax({
        url: added_function.ajax_url,
        type: 'post',
        data: {
            action: 'post_remove_function',
            post_id: post_id,
            post_data_id : post_data_id
        },
        beforeSend: function () {
            jQuery('.custom-dropdown-loader').removeClass('custom-dropdown-loader-hidden');

        },
        success: function (response) {
            var obj = jQuery.parseJSON(response);
            console.log(obj);
            jQuery('.custom-dropdown-loader').addClass('custom-dropdown-loader-hidden');
            jQuery('#cart-product-' + obj.id).remove();
            jQuery('.custom-cart-totals').html(obj.total);
            var link = jQuery('.custom-cart-link').attr('href');
            location.href = link;
        }
    });
}


var search_cont = false;
jQuery('#search_mobile').on('click touch', function () {
    "use strict";
    if (search_cont === false) {
        jQuery('.the-search-mobile').removeClass('search-mobile-hidden');
        jQuery('.the-search-mobile').addClass('search-mobile-shown');
        jQuery('#search_mobile').removeClass('fa-search');
        jQuery('#search_mobile').addClass('fa-times');
        search_cont = true;
    } else {
        jQuery('.the-search-mobile').removeClass('search-mobile-shown');
        jQuery('.the-search-mobile').addClass('search-mobile-hidden');
        jQuery('#search_mobile').addClass('fa-search');
        jQuery('#search_mobile').removeClass('fa-times');
        search_cont = false;
    }
});


jQuery(document.body).on('updated_cart_totals', function (e) {
    "use strict";
    e.preventDefault;
    jQuery.ajax({
        url: added_function.ajax_url,
        type: 'post',
        data: {
            action: 'post_update_function'
        },
        beforeSend: function () {
            console.log('aqui estoy');
            jQuery('.custom-cart-products-container').html('');
            jQuery('.custom-dropdown-loader').removeClass('custom-dropdown-loader-hidden');
        },
        success: function (response) {
            jQuery('.custom-cart-products-container').html(response);
            jQuery('.custom-dropdown-loader').addClass('custom-dropdown-loader-hidden');
        }
    });
});

function clearcart() {
    "use strict";
    jQuery.ajax({
        url: added_function.ajax_url,
        type: 'post',
        data: {
            action: 'post_empty_function'
        },
        beforeSend: function () {
            jQuery('.custom-dropdown-loader').removeClass('custom-dropdown-loader-hidden');
        },
        success: function (response) {
            jQuery('.custom-dropdown-loader').addClass('custom-dropdown-loader-hidden');
            jQuery('.custom-dropdown-menu').hide();
            location.reload();
        }
    });
}
