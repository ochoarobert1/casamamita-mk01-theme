<footer class="container-fluid" role="contentinfo" itemscope itemtype="http://schema.org/WPFooter">
    <div class="row">
        <?php if (!is_home()) { ?>
        <section class="mobile-social-section mobile-social-section-inner col-xs-12 hidden-lg hidden-md hidden-sm no-paddingl no-paddingr">
            <a href="https://www.facebook.com/CASA-Mamita-504807906288442/" target="_blank"><i class="fa fa-facebook-square"></i></a>
            <a href="https://www.instagram.com/casamamita/" target="_blank"><i class="fa fa-instagram"></i></a>
        </section>
        <?php } ?>
        <section class="the-bulletin col-lg-12 col-md-12 col-sm-12 col-xs-12">
            <div class="container">
                <div class="row">
                    <div class="bulletin-container col-lg-12 col-md-12 col-sm-12 col-xs-12">
                        <h2>
                            <?php _e('BOLETIN', 'casamamita'); ?>
                        </h2>
                        <p>
                            <?php _e('No te pierdas de nuestras promociones, suscribite', 'casamamita'); ?>
                        </p>
                        <div class="col-md-8 col-md-offset-2">
                            <div id="casamamita_5678444713082880" class="agile_crm_form_embed"><span style="display:none">Fill out my <a href="https://casamamita.agilecrm.com/forms/5678444713082880">online form</a></span></div>
                        </div>
                        <div class="clearfix"></div>
                    </div>
                </div>
            </div>
        </section>
        <div class="the-footer col-lg-12 col-md-12 col-sm-12 col-xs-12 no-paddingl no-paddingr">
            <div class="container">
                <div class="row">
                    <div class="footer-item col-md-2 col-sm-2 col-xs-12">
                        <div class="hidden-xs">
                            <h3>
                                <?php _e('EMPRESA', 'casamamita'); ?>
                            </h3>
                            <?php wp_nav_menu(array('container_class' => 'menu-footer', 'theme_location' => 'menu_footer', 'items_wrap' => '<ul id="%1$s" class="%2$s">%3$s</ul>' )); ?>
                        </div>
                        <div class="hidden-lg hidden-md hidden-sm">
                            <div class="panel-group panel-mobile-menu" id="accordion" role="tablist" aria-multiselectable="true">
                                <div class="panel panel-default">
                                    <div class="panel-heading" role="tab" id="headingOne">
                                        <h4 class="panel-title collapsed" role="button" data-toggle="collapse" data-parent="#accordion" href="#collapseOne" aria-expanded="true" aria-controls="collapseOne">
                                            <a>
                                                <?php _e('MI CUENTA', 'casamamita'); ?> <span class="mobile-menu-helper"><i class="fa fa-chevron-up"></i></span>
                                            </a>
                                        </h4>
                                    </div>
                                    <div id="collapseOne" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingOne">
                                        <div class="panel-body">
                                            <?php wp_nav_menu(array('container_class' => 'menu-footer', 'theme_location' => 'menu_footer', 'items_wrap' => '<ul id="%1$s" class="list-items-mobile %2$s">%3$s</ul>' )); ?>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="footer-item col-md-2 col-sm-2 col-xs-12">
                        <div class="hidden-xs">
                            <h3>
                                <?php _e('SOBRE NOSOTROS', 'casamamita'); ?>
                            </h3>
                            <?php wp_nav_menu(array('container_class' => 'menu-footer', 'theme_location' => 'menu_footer_2nd', 'items_wrap' => '<ul id="%1$s" class="%2$s">%3$s</ul>' )); ?>
                        </div>
                        <div class="hidden-lg hidden-md hidden-sm">
                            <div class="panel-group panel-mobile-menu" id="accordion2" role="tablist" aria-multiselectable="true">
                                <div class="panel panel-default">
                                    <div class="panel-heading" role="tab" id="headingTwo">
                                        <h4 class="panel-title collapsed" role="button" data-toggle="collapse" data-parent="#accordion2" href="#collapseTwo" aria-expanded="true" aria-controls="collapseTwo">
                                            <a>
                                                <?php _e('INFORMACIÓN', 'casamamita'); ?> <span class="mobile-menu-helper"><i class="fa fa-chevron-up"></i></span>
                                            </a>
                                        </h4>
                                    </div>
                                    <div id="collapseTwo" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingTwo">
                                        <div class="panel-body">
                                            <?php wp_nav_menu(array('container_class' => 'menu-footer', 'theme_location' => 'menu_footer_2nd', 'items_wrap' => '<ul id="%1$s" class="list-items-mobile %2$s">%3$s</ul>' )); ?>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="footer-item col-md-2 col-sm-2 hidden-xs">
                        <h3>
                            <?php _e('MARCAS PRINCIPALES', 'casamamita'); ?>
                        </h3>
                        <ul>

                            <?php $marcas = get_term_by( 'slug', 'marcas', 'product_cat' ); ?>
                            <?php $terms = get_terms( array('hide_empty' => false, 'number' => 5, 'taxonomy' => 'product_cat', 'child_of' => $marcas->term_id )); ?>
                            <?php if (!empty($terms)) { ?>
                            <?php foreach ($terms as $items) { ?>
                            <li><a href="<?php echo get_term_link($items->term_id, 'product_cat'); ?>"><?php echo $items->name; ?></a></li>
                            <?php } } ?>
                        </ul>
                    </div>
                    <div class="footer-item col-md-2 col-sm-2 hidden-xs">
                        <h3>
                            <?php _e('RECONOCIMIENTOS', 'casamamita'); ?>
                        </h3>
                        <img src="<?php echo esc_url(get_template_directory_uri()); ?>/images/trophy.png" alt="" class="img-responsive" />
                    </div>
                    <div class="footer-item col-md-2 col-sm-2 hidden-xs">
                        <h3>
                            <?php _e('metodos de pago', 'casamamita'); ?>
                        </h3>
                        <img src="<?php echo esc_url(get_template_directory_uri()); ?>/images/payment.png" alt="" class="img-responsive" />
                    </div>
                    <div class="footer-item col-md-2 col-sm-2 hidden-xs no-paddingr">
                        <h3>
                            <?php _e('Contáctanos', 'casamamita'); ?>
                        </h3>
                        <p>Rioja 1124 - Rosario - Santa Fe - Argentina</p>
                        <p><strong><i class="fa fa-phone"></i> (341) 449-81 25</strong></p>
                        <p><strong>Lunes a Viernes:</strong> 9.00 a 13.00 hs. / 15.30 a 19.30 hs. <strong>Sabados:</strong> 9:00 hs. a 13:00 hs.</p>
                        <p><a href="mailto:info@casamamita.com">info@casamamita.com</a></p>
                    </div>
                    <div class="clearfix"></div>
                    <div class="footer-copy col-md-8 col-md-offset-2 hidden-xs">
                        <hr>
                        <img src="<?php echo esc_url(get_template_directory_uri()); ?>/images/logo.png" alt="" class="img-responsive" />
                        <div class="social-container">
                            <a href="https://www.facebook.com/CASA-Mamita-504807906288442/" target="_blank"><i class="fa fa-facebook"></i></a>
                            <a href="https://www.instagram.com/casamamita/" target="_blank"><i class="fa fa-instagram"></i></a>
                        </div>
                        <h6>
                            <?php _e('Casamamita online creada con Woocommerce', 'casamamita'); ?>
                        </h6>
                    </div>
                    <div class="footer-copy-mobile hidden-lg hidden-md hidden-sm col-xs-12">

                        <p>Rioja 1124 - Rosario - Santa Fe - Argentina</p>
                        <h3><strong><i class="fa fa-phone"></i> (341) 449-81 25</strong></h3>
                        <p><strong>Lunes a Viernes:</strong> 9.00 a 13.00 hs. / 15.30 a 19.30 hs. <strong>Sabados:</strong> 9:00 hs. a 13:00 hs.</p>
                        <p><a href="mailto:info@casamamita.com">info@casamamita.com</a></p>
                        <div class="social-container">
                            <a href="https://www.facebook.com/CASA-Mamita-504807906288442/" target="_blank"><i class="fa fa-facebook"></i></a>
                            <a href="https://www.instagram.com/casamamita/" target="_blank"><i class="fa fa-instagram"></i></a>
                        </div>
                        <h5>
                            <a href="mailto:info@casamamita.com" title="<?php _e('Envíenos un correo','casamamita')?>"></a>
                        </h5>
                        <h6>
                            <?php _e('Casamamita online creada con Woocommerce', 'casamamita'); ?>
                        </h6>
                    </div>
                </div>
            </div>
        </div>
    </div>
</footer>
<?php wp_footer() ?>

</body>

</html>
