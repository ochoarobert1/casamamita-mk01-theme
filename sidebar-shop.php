<?php if ( is_active_sidebar( 'shop_sidebar' ) ) : ?>
<ul id="sidebar_shop">
    <?php dynamic_sidebar( 'shop_sidebar' ); ?>
</ul>
<?php endif; ?>
