<!DOCTYPE html>
<html <?php language_attributes() ?>>
    <head>
        <?php /* MAIN STUFF */ ?>
        <meta http-equiv="X-UA-Compatible" content="IE=edge" />
        <meta http-equiv="Content-Type" content="text/html; charset=<?php bloginfo('charset') ?>" />
        <meta name="robots" content="NOODP, INDEX, FOLLOW" />
        <meta name="HandheldFriendly" content="True" />
        <meta name="MobileOptimized" content="320" />
        <meta name="viewport" content="width=device-width, initial-scale=1" />
        <link rel="pingback" href="<?php echo esc_url(get_bloginfo('pingback_url')); ?>" />
        <link rel="profile" href="http://gmpg.org/xfn/11" />
        <link rel="dns-prefetch" href="//connect.facebook.net" />
        <link rel="dns-prefetch" href="//facebook.com" />
        <link rel="dns-prefetch" href="//googleads.g.doubleclick.net" />
        <link rel="dns-prefetch" href="//pagead2.googlesyndication.com" />
        <link rel="dns-prefetch" href="//google-analytics.com" />
		<meta name="google-site-verification" content="<meta name='google-site-verification' content='IVxNEkbSpC0m0r0YMtRRcGbzxyuNsiGmwsbJ9duL9sg' />" />
        <?php /* FAVICONS */ ?>
        <link rel="apple-touch-icon" href="<?php echo get_template_directory_uri(); ?>/images/apple-touch-icon.png" />
        <?php /* THEME NAVBAR COLOR */ ?>
        <meta name="msapplication-TileColor" content="#FF1950" />
        <meta name="msapplication-TileImage" content="<?php echo get_template_directory_uri(); ?>/images/win8-tile-icon.png" />
        <meta name="theme-color" content="#FF1950" />
        <?php /* AUTHOR INFORMATION */ ?>
        <meta name="language" content="<?php echo get_bloginfo('language'); ?>" />
        <meta name="author" content="Casamamita" />
        <meta name="copyright" content="http://casamamita.com" />
        <meta name="geo.position" content="-32.9474955,-60.6409407" />
        <meta name="ICBM" content="-32.9474955,-60.6409407" />
        <meta name="geo.region" content="AR" />
        <meta name="geo.placename" content="Rioja 1124 Rosario, Santa Fe Argentina" />
        <meta name="DC.title" content="<?php if (is_home()) { echo get_bloginfo('name') . ' | ' . get_bloginfo('description'); } else { echo get_the_title() . ' | ' . get_bloginfo('name'); } ?>" />
        <?php /* MAIN TITLE - CALL HEADER MAIN FUNCTIONS */ ?>
        <?php wp_title('|', false, 'right'); ?>
        <?php wp_head() ?>
        <?php /* OPEN GRAPHS INFO - COMMENTS SCRIPTS */ ?>
        <?php get_template_part('includes/header-oginfo'); ?>
        <?php if ( is_singular() && get_option( 'thread_comments' ) ) wp_enqueue_script( 'comment-reply' ); ?>
        <?php /* IE COMPATIBILITIES */ ?>
        <!--[if lt IE 7]><html <?php language_attributes(); ?> class="no-js lt-ie9 lt-ie8 lt-ie7" /><![endif]-->
        <!--[if (IE 7)&!(IEMobile)]><html <?php language_attributes(); ?> class="no-js lt-ie9 lt-ie8" /><![endif]-->
        <!--[if (IE 8)&!(IEMobile)]><html <?php language_attributes(); ?> class="no-js lt-ie9" /><![endif]-->
        <!--[if gt IE 8]><!-->
        <html <?php language_attributes(); ?> class="no-js" />
        <!--<![endif]-->
        <!--[if IE]> <script type="text/javascript" src="https://cdn.jsdelivr.net/html5shiv/3.7.2/html5shiv.min.js"></script> <![endif]-->
        <!--[if IE]> <script type="text/javascript" src="https://cdn.jsdelivr.net/respond/1.4.2/respond.min.js"></script> <![endif]-->
        <!--[if IE]> <link rel="shortcut icon" href="<?php echo get_template_directory_uri(); ?>/favicon.ico" /> <![endif]-->
        <?php get_template_part('includes/fb-script'); ?>
        <?php get_template_part('includes/ga-script'); ?>
    </head>

    <body class="the-main-body <?php echo join(' ', get_body_class()); ?>" itemscope itemtype="http://schema.org/WebPage">
        <div id="fb-root"></div>
        <div class="the-notices the-notices-hidden">
            <img src="<?php echo esc_url(get_template_directory_uri()); ?>/images/added.png" alt="" class="img-responsive" />
            <span><?php _e('Agregado al Carrito', 'casamamita'); ?></span>
            <?php do_action('custom_woocommerce_notices'); ?>
        </div>
        <header class="container-fluid" role="banner" itemscope itemtype="http://schema.org/WPHeader">
            <div class="row">
                <div class="the-header-mobile hidden-lg hidden-md hidden-sm col-xs-12 no-paddingl no-paddingr">
                    <div id="sticker-mobile" class="pre-header-mobile col-xs-12 no-paddingl no-paddingr">
                        <div class="pre-header-menu col-xs-6 no-paddingl">
                            <div class="btn-group">
                                <button type="button" class="btn btn-default dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                    <i class="fa fa-ellipsis-h" aria-hidden="true"></i>
                                </button>
                                <?php $theme_locations = get_nav_menu_locations(); ?>
                                <?php $menu_obj = get_term( $theme_locations['preheader_menu'] ); ?>
                                <?php $menu_container = wp_get_menu_array($menu_obj); ?>
                                <ul class="dropdown-menu dropdown-mobile-menu">
                                    <?php foreach ($menu_container as $items) {?>
                                    <?php if ($items['title'] == 'Iniciar Sesión') { ?>
                                    <?php  if ( is_user_logged_in() ) { $current_user = wp_get_current_user(); ?>
                                    <li><a href="<?php echo $items['url']; ?>"><?php echo 'Hola: ' . $current_user->display_name; ?></a></li>
                                    <li><a href="<?php echo wp_logout_url( home_url() ); ?>"><?php echo 'Cerrar Sesión'; ?></a></li>
                                    <?php } else { ?>
                                    <li><a href="<?php echo $items['url']; ?>"><?php echo $items['title']; ?></a></li>
                                    <?php } ?>
                                    <?php } else { ?>
                                    <li><a href="<?php echo $items['url']; ?>"><?php echo $items['title']; ?></a></li>
                                    <?php } ?>
                                    <?php } ?>
                                </ul>

                            </div>
                        </div>
                        <div class="pre-header-elements col-xs-6 no-paddingl">
                            <div class="btn-group">
                                <?php global $woocommerce; ?>
                                <?php $items = $woocommerce->cart->get_cart(); ?>
                                <?php $cart_url = $woocommerce->cart->get_cart_url(); ?>
                                <?php if ( WC()->cart->get_cart_contents_count() == 0 ) { ?>
                                <div class="empty-cart custom-cart-mobile">
                                    <a href="<?php echo esc_url($cart_url); ?>">
                                        <i class="custom-cart-icon"></i>
                                        <span><?php _e('VACIO','casamamita'); ?></span>
                                    </a>
                                </div>
                                <?php } else { ?>

                                <div class="custom-cart custom-cart-mobile">
                                    <a href="<?php echo esc_url($cart_url); ?>">
                                        <i class="custom-cart-icon"></i>
                                        <span class="badge"> <?php echo count($items); ?></span>
                                    </a>
                                </div>
                                <?php } ?>
                            </div>
                        </div>
                    </div>
                    <div class="the-logo-mobile col-xs-12 no-paddingl no-paddingr">
                        <div class="col-xs-8 col-xs-offset-2">
                            <a href="<?php echo home_url('/'); ?>"><img src="<?php echo esc_url(get_template_directory_uri()); ?>/images/logo.png" alt="" class="img-responsive" /></a>
                        </div>
                    </div>
                    <div class="the-navbar-mobile col-xs-12 no-paddingl no-paddingr">
                        <div class="btn-group">
                            <button type="button" class="btn btn-default dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                <i class="fa fa-bars"></i> <span>MENU</span>
                            </button>
                            <ul class="dropdown-menu dropdown-mobile-menu dropdown-mobile-menu-main">
                                <?php $menu_mobile = wp_get_menu_array('Menu Principal'); ?>
                                <?php foreach ($menu_mobile as $items) { ?>
                                <li>
                                    <a href="<?php echo $items['url']; ?>">
                                        <?php echo $items['title']; ?>
                                    </a>
                                </li>
                                <?php } ?>
                            </ul>

                        </div>
                        <i id="search_mobile" class="fa fa-search pull-right"></i>
                    </div>
                    <div class="the-search-mobile search-mobile-hidden col-xs-12 no-paddingl no-paddingr">
                        <?php get_search_form(); ?>
                    </div>
                </div>
                <div class="the-header col-lg-12 col-md-12 col-sm-12 hidden-xs no-paddingl no-paddingr">
                    <div id="sticker" class="pre-header col-lg-12 col-md-12 col-sm-12 col-xs-12 no-paddingl no-paddingr">
                        <div class="container">
                            <div class="row">
                                <div class="pre-header-side-content col-lg-6 col-md-6 col-sm-8 col-xs-6">
                                    <div class="pre-header-menu-container col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                        <?php $theme_locations = get_nav_menu_locations(); ?>
                                        <?php $menu_obj = get_term( $theme_locations['preheader_menu'] ); ?>
                                        <?php $menu_container = wp_get_menu_array($menu_obj); ?>
                                        <ul class="nav custom-nav">
                                            <?php foreach ($menu_container as $items) {?>
                                            <?php if ($items['title'] == 'Iniciar Sesión') { ?>
                                            <?php  if ( is_user_logged_in() ) { $current_user = wp_get_current_user(); ?>
                                            <li><a href="<?php echo $items['url']; ?>"><?php echo 'Hola: ' . $current_user->display_name; ?></a></li>
                                            <li><a href="<?php echo wp_logout_url( home_url() ); ?>"><?php echo 'Cerrar Sesión'; ?></a></li>
                                            <?php } else { ?>
                                            <li><a href="<?php echo $items['url']; ?>"><?php echo $items['title']; ?></a></li>
                                            <?php } ?>
                                            <?php } else { ?>
                                            <li><a href="<?php echo $items['url']; ?>"><?php echo $items['title']; ?></a></li>
                                            <?php } ?>
                                            <?php } ?>
                                        </ul>
                                    </div>
                                </div>
                                <div class="pre-header-side-content col-lg-6 col-md-6 col-sm-4 col-xs-6">
                                    <div class="preheader-side-button-container col-lg-5 col-md-5 col-sm-10 col-xs-8 pull-right no-paddingr">
                                        <div class="btn-group">
                                            <?php global $woocommerce; ?>
                                            <?php $items = $woocommerce->cart->get_cart(); ?>
                                            <?php $cart_url = $woocommerce->cart->get_cart_url(); ?>
                                            <?php if ( WC()->cart->get_cart_contents_count() == 0 ) { ?>
                                            <div class="empty-cart">
                                                <a href="<?php echo esc_url($cart_url); ?>">
                                                    <i class="custom-cart-icon"></i>
                                                    <span><?php _e('CARRITO','casamamita'); ?></span>
                                                    <span><?php _e('VACIO','casamamita'); ?></span>
                                                </a>
                                            </div>
                                            <?php } else { ?>

                                            <div class="custom-cart">
                                                <div class="custom-cart-opener">
                                                    <i class="custom-cart-icon"></i>
                                                    <span><?php _e('CARRITO','casamamita'); ?></span>
                                                    <span class="caret"></span>
                                                    <span class="badge"> <?php echo count($items); ?></span>
                                                </div>
                                                <ul class="custom-dropdown-menu custom-dropdown-hide">
                                                    <li class="custom-dropdown-loader custom-dropdown-loader-hidden">
                                                        <div class="loader"></div>
                                                    </li>
                                                    <?php $total = 0; ?>
                                                    <div class="custom-cart-products-container">
                                                        <?php foreach($items as $item => $values) { ?>

                                                        <?php $_product = $values['data']->post; ?>

                                                        <li id="cart-product-<?php echo $values['variation_id']; ?>">
                                                            <a href="<?php echo get_permalink($_product); ?>" title="<?php echo $_product->post_title; ?>">
                                                                <?php $post_url = get_the_post_thumbnail_url( $_product, 'avatar' ); ?>
                                                                <img src="<?php echo $post_url; ?>" alt="<?php echo $_product->post_title; ?>" class="img-responsive img-cart" />
                                                                <p><?php echo $_product->post_title; ?></p>
                                                                <?php $regular_price = get_post_meta( $values['product_id'], '_regular_price', true); ?>
                                                                <?php if ($regular_price == ""){ ?>
                                                                <?php $price = $values['line_total']; ?>
                                                                <?php $variations = $values['variation']; ?>
                                                                <?php foreach ($variations as $varia_item) { ?>
                                                                <p><?php echo '<strong>Color:</strong> ' . $varia_item; ?></p>
                                                                <?php } ?>
                                                                <?php } else { ?>
                                                                <?php $price = (get_post_meta($values['product_id'] , '_price', true) * $values['quantity']); ?>

                                                                <?php } ?>

                                                                <span>
                                                                    <?php _e('Cantidad: ', 'casamamita'); ?>
                                                                    <?php echo $values['quantity']; ?>
                                                                    <?php _e('Total: ', 'casamamita'); ?>
                                                                    <?php echo get_woocommerce_currency_symbol() . ' ' . number_format($price, 2, ',', ' '); ?>
                                                                </span>
                                                            </a>
                                                            <a onclick="remove_cart('<?php echo $values['variation_id']; ?>', '<?php echo esc_attr( $item ); ?>')" class="custom-remove-product"><i class="fa fa-times"></i></a>
                                                            <div class="clearfix"></div>
                                                        </li>
                                                        <?php } ?>
                                                        <li class="custom-cart-button">
                                                            <div class="custom-cart-totals">
                                                                <?php _e('Total: ', 'casamamita'); ?>
                                                                <?php $total = $woocommerce->cart->get_cart_total(); ?>
                                                                <?php echo $total; ?>

                                                            </div>
                                                            <a href="<?php echo esc_url($cart_url); ?>" class="custom-cart-link"><button class="btn btn-md btn-custom-cart"><?php _e('Ir al Carrito', 'casamamita'); ?></button></a>
                                                            <a onclick="clearcart()" class="custom-cart-link"><button class="btn btn-md btn-custom-cart btn-custom-cart-clear"><?php _e('Vaciar Carrito', 'casamamita'); ?></button></a>
                                                        </li>
                                                    </div>
                                                </ul>
                                            </div>
                                            <?php } ?>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="the-logo col-lg-12 col-md-12 col-sm-12 col-xs-12">
                        <div class="col-md-4 col-md-offset-4">
                            <a href="<?php echo home_url('/'); ?>"><img src="<?php echo esc_url(get_template_directory_uri()); ?>/images/logo.png" alt="" class="img-responsive" /></a>
                        </div>
                    </div>
                    <div class="the-search col-lg-12 col-md-12 col-sm-12 col-xs-12">
                        <div class="container">
                            <div class="row">
                                <div class="col-md-8 col-md-offset-2">
                                    <?php get_search_form(); ?>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="the-navbar col-lg-12 col-md-12 col-sm-12 col-xs-12 no-paddingl no-paddingr">
                        <div class="container">
                            <div class="row">
                                <div class="custom-navbar-menu col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                    <?php $theme_locations = get_nav_menu_locations(); ?>
                                    <?php $menu_obj = get_term( $theme_locations['header_menu'] ); ?>
                                    <?php echo special_menu_container($menu_obj->name); ?>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </header>
