<form role="search" method="get" id="searchform" class="searchform" action="<?php echo home_url( '/' ); ?>">
    <div>
        <div class="input-group">
            <input type="search" id="s" name="s" value="" class="form-control" placeholder="<?php _e('Busca lo mejor para tu bebé...','casamamita'); ?>">
            <input type="hidden" name="post_type" value="product" />
            <span class="input-group-btn">
                <button  type="submit" id="searchsubmit"  class="btn btn-default" type="button"><span class="glyphicon glyphicon-search"></span></button>
            </span>
        </div><!-- /input-group -->
    </div>
</form>