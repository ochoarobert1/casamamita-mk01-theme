<?php
function special_menu_container($menu_special) {
?>
<ul class="nav_main">
    <?php $arrayitems = wp_get_menu_array($menu_special); ?>
    <?php $i = 1; ?>
    <?php foreach ($arrayitems as $items) {?>
    <li class="nav_main_<?php echo $i; ?>" id="<?php echo $i; ?>">
        <?php $item_term = $items['url']; ?>
        <?php $variable = explode('/categoria-producto/', $item_term); ?>
        <?php $item_var = $variable[1]; ?>
        <?php $term_father = get_term_by( 'slug',  strstr($item_var, '/', true), 'product_cat'); ?>
        <?php if (!empty($term_father)) { ?>
        <?php $term_subfather = get_terms( array( 'taxonomy' => 'product_cat', 'parent' => $term_father->term_id, 'hide_empty' => 0, 'order' => 'ASC', 'orderby' => 'term_group' ) ); ?>
        <?php if ( ! empty( $term_subfather ) && ! is_wp_error( $term_subfather ) ) { ?>
        <a class="submenu-link" href="<?php echo $items['url']; ?>">
            <?php echo $items['title']; ?>
        </a>
        <ul id="dropdown-custom-<?php echo $i; ?>" class="dropdown-custom">

            <?php $term_subfather = get_terms( array( 'taxonomy' => 'product_cat', 'parent' => $term_father->term_id, 'hide_empty' => 0, 'order' => 'ASC', 'orderby' => 'term_group' ) ); ?>
            <?php $num_term = count($term_subfather); ?>
            <div class="dropdown-custom-child-slider owl-carousel owl-theme">
                <?php foreach ($term_subfather as $term_subfather_items) { ?>
                <div class="dropdown-custom-child-item">
                    <?php ?>
                    <h4><a href="<?php echo get_term_link( $term_subfather_items );?>"><?php echo $term_subfather_items->name; ?></a></h4>
                    <ul class="dropdown-child-item">
                        <?php $term_children = get_terms( array( 'taxonomy' => 'product_cat', 'child_of' => $term_subfather_items->term_id, 'hide_empty' => 0, 'order' => 'ASC', 'orderby' => 'slug' ) ); ?>
                        <?php foreach ($term_children as $term_children_items) { ?>
                        <li><a href="<?php echo get_term_link( $term_children_items->term_id, 'product_cat' );?>"><?php echo $term_children_items->name; ?> (<?php echo $term_children_items->count; ?>)</a></li>
                        <?php } ?>
                    </ul>
                </div>
                <?php } ?>
            </div>
            <div class="dropdown-custom-child-item dropdown-custom-child-item-featured">
                <h4><?php _e('POPULARES', 'casamamita'); ?></h4>
                <?php $args = array('post_type' => 'product', 'posts_per_page' => 1, 'order' => 'DESC', 'orderby' => 'date', 'tax_query' => array( array( 'taxonomy' => 'product_cat', 'field' => 'term_id', 'terms' => $term_father->term_id ) ) ); ?>
                <?php $q = new WP_Query($args); ?>
                <?php if ( $q->have_posts() ) : ?>
                <?php while ($q->have_posts()) : $q->the_post(); ?>
                <?php $product = new WC_Product( get_the_ID() ); ?>
                <article class="menu-product-item item">
                    <picture>
                        <a href="<?php the_permalink(); ?>">
                            <?php the_post_thumbnail('product_img', array('class' => 'img-responsive')); ?>
                        </a>
                    </picture>
                    <header>
                        <h3>
                            <?php the_title(); ?>
                        </h3>
                    </header>
                    <div class="home-product-item-info">
                        <p>
                            <?php echo $product->get_price_html(); ?>
                        </p>
                    </div>
                </article>
                <?php endwhile; ?>
                <?php endif; ?>
                <?php wp_reset_query(); ?>
            </div>

            <div class="clearfix"></div>
            <?php $query_helper = strstr($item_var, '/', true); ?>
            <?php $args = array('post_type' => 'promociones', 'posts_per_page' => 2, 'order' => 'DESC', 'orderby' => 'date', 'meta_query' => array( array( 'key' => 'rw_promo_category', 'value' => $query_helper, 'compare' => 'LIKE'))); ?>
            <?php $q2 = new WP_Query($args); ?>
            <?php if ( $q2->have_posts() ) : ?>
            <?php while ($q2->have_posts()) : $q2->the_post(); ?>
            <div class="dropdown-banner-container">
                <a href="<?php echo get_post_meta(get_the_ID(), 'rw_promo_link', 'true'); ?>">
                    <?php the_post_thumbnail('full', array('class' => 'img-responsive')); ?>
                </a>
            </div>
            <?php endwhile; ?>
            <?php endif; ?>
            <?php wp_reset_query(); ?>

        </ul>
        <?php } else { ?>
        <a href="<?php echo $items['url']; ?>" class="submenu-link">
            <?php echo $items['title']; ?>
        </a>
        <?php } ?>
        <?php } else { ?>
        <a href="<?php echo $items['url']; ?>" class="submenu-link">
            <?php echo $items['title']; ?>
        </a>
        <?php } ?>
    </li>
    <?php $i++; } ?>
</ul>
<?php
                                               }
