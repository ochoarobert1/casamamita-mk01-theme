<?php
/* WOOCOMMERCE CUSTOM COMMANDS */

/* WOOCOMMERCE - DECLARE THEME SUPPORT - BEGIN */
add_action( 'after_setup_theme', 'woocommerce_support' );
function woocommerce_support() {
    add_theme_support( 'woocommerce' );
}
/* WOOCOMMERCE - DECLARE THEME SUPPORT - END */

/* WOOCOMMERCE - CUSTOM WRAPPER - BEGIN */
remove_action( 'woocommerce_before_main_content', 'woocommerce_output_content_wrapper', 10);
remove_action( 'woocommerce_after_main_content', 'woocommerce_output_content_wrapper_end', 10);

add_action('woocommerce_before_main_content', 'my_theme_wrapper_start', 10);
add_action('woocommerce_after_main_content', 'my_theme_wrapper_end', 10);

function my_theme_wrapper_start() {
    echo '<section id="main" class="container-fluid"><div class="row"><div class="woocustom-main-container col-lg-12 col-md-12 col-sm-12 col-xs-12 no-paddingl no-paddingr">';
}

function my_theme_wrapper_end() {
    echo '</div></div></section>';
}
/* WOOCOMMERCE - CUSTOM WRAPPER - END */

/* WOOCOMMERCE - CUSTOM HOOKS - BEGIN */



/* BREADCRUMBS */

add_filter( 'woocommerce_breadcrumb_defaults', 'jk_change_breadcrumb_delimiter' );
function jk_change_breadcrumb_delimiter( $defaults ) {
    // Change the breadcrumb delimeter from '/' to '>'
    $defaults['delimiter'] = ' | ';
    return $defaults;
}

/* SINGLE PRODUCT */
remove_action( 'woocommerce_single_product_summary', 'woocommerce_template_single_excerpt', 20);
remove_action( 'woocommerce_single_product_summary', 'woocommerce_template_single_meta', 40);
add_action('woocommerce_single_product_summary', 'woocommerce_custom_single_meta', 15);

function woocommerce_custom_single_meta() {
    echo '<div class="sku-container">';
    echo '<span class="sku-text">';
    _e('REFERENCIA: ', 'casamamita');
    echo '</span>';
    global $product;
    echo $product->get_sku();
    echo '</div>';


}

add_action('woocommerce_single_product_summary', 'woocommerce_custom_condicion', 17);

function woocommerce_custom_condicion() {
    echo '<div class="attribute-container">';
    echo '<span class="attribute-text">';
    _e('CONDICIÓN: ', 'casamamita');
    echo '</span>';
    global $product;
    $attributes = $product->get_attributes();
    foreach ( $attributes as $attribute ) {
        if ( $attribute['name'] == 'pa_condicion' ) {
            $values = wc_get_product_terms( $product->id, $attribute['name'], array( 'fields' => 'names' ) );
            echo $values[0];
        } }
    echo '</div>';
}

remove_action( 'woocommerce_single_product_summary', 'woocommerce_template_single_rating', 10);
add_action('woocommerce_single_product_summary', 'woocommerce_template_single_rating', 25);

add_action( 'woocommerce_share', 'custom_woocommerce_sharer', 10 );
function custom_woocommerce_sharer() {
    echo '<div class="custom-sharer-container">';

    echo '</div>';
}


add_filter( 'woocommerce_product_tabs', 'woo_rename_tabs', 98 );
function woo_rename_tabs( $tabs ) {
    $tabs['description']['title'] = esc_html__( 'Mas', 'casamamita' );        // Rename the description tab
    $tabs['reviews']['title'] = esc_html__( 'Reseñas', 'casamamita' );
    $tabs['additional_information']['title'] = esc_html__( 'Ficha Técnica', 'casamamita' );    // Rename the additional
    return $tabs;
}

add_filter( 'woocommerce_product_tabs', 'woo_reorder_tabs', 98 );
function woo_reorder_tabs( $tabs ) {

    $tabs['reviews']['priority'] = 20;            // Reviews first
    $tabs['description']['priority'] = 10;            // Description second
    $tabs['additional_information']['priority'] = 15;    // Additional information third

    return $tabs;
}



remove_action( 'woocommerce_after_single_product_summary', 'woocommerce_output_related_products', 20);
add_action('woocommerce_custom_related_product', 'woocommerce_output_related_products', 1);

//add_action( 'custom_woocommerce_notices', 'wc_print_notices', 1 );

add_filter( 'woocommerce_product_add_to_cart_text' , 'custom_woocommerce_product_add_to_cart_text' );
/**
 * custom_woocommerce_template_loop_add_to_cart
*/
function custom_woocommerce_product_add_to_cart_text() {
    global $product;

    $product_type = $product->product_type;

    switch ( $product_type ) {
        case 'external':
            return __( 'Ver producto', 'casamamita' );
            break;
        case 'grouped':
            return __( 'Ver producto', 'casamamita' );
            break;
        case 'simple':
            return __( 'Ver producto', 'casamamita' );
            break;
        case 'variable':
            return __( 'Ver producto', 'casamamita' );
            break;
        default:
            return __( 'Ver producto', 'casamamita' );
    }

}




/**
 *Reduce the strength requirement on the woocommerce password.
 *
 * Strength Settings
 * 3 = Strong (default)
 * 2 = Medium
 * 1 = Weak
 * 0 = Very Weak / Anything
 */
//function reduce_woocommerce_min_strength_requirement( $strength ) {
//    return 0;
//}
//add_filter( 'woocommerce_min_password_strength', 'reduce_woocommerce_min_strength_requirement' );

function wc_ninja_remove_password_strength() {
    if ( wp_script_is( 'wc-password-strength-meter', 'enqueued' ) ) {
        wp_dequeue_script( 'wc-password-strength-meter' );
    }
}
add_action( 'wp_print_scripts', 'wc_ninja_remove_password_strength', 100 );



remove_action('woocommerce_after_shop_loop_item', 'woocommerce_template_loop_add_to_cart', 10);
add_action('woocommerce_after_shop_loop_item', 'woocommerce_custom_loop_add_to_cart', 10);
function woocommerce_custom_loop_add_to_cart() {
    global $product;
    $link = $product->get_permalink();
    echo '<a href="' . esc_attr($link) . '" class="btn btn-md btn-product-shop">Ver Producto</a>';

}
/* WOOCOMMERCE - CUSTOM HOOKS - END */

add_action( 'after_setup_theme', 'yourtheme_setup' );

function yourtheme_setup() {
    add_theme_support( 'wc-product-gallery-zoom' );
    add_theme_support( 'wc-product-gallery-lightbox' );
    add_theme_support( 'wc-product-gallery-slider' );
}

?>


