<?php
/* --------------------------------------------------------------
    ADD CUSTOM WORDPRESS FUNCTIONS
-------------------------------------------------------------- */

require_once('includes/wp_functions_overrides.php');

/* --------------------------------------------------------------
    ENQUEUE AND REGISTER CSS
-------------------------------------------------------------- */

function casamamita_load_css() {
    if (!is_admin()){
        $version_remove = NULL;
        if ($_SERVER['REMOTE_ADDR'] == '::1') {

            /*- BOOTSTRAP CORE ON LOCAL -*/
            wp_register_style('bootstrap-css', get_template_directory_uri() . '/css/bootstrap.min.css', false, '3.3.7', 'all');
            wp_enqueue_style('bootstrap-css');

            /*- BOOTSTRAP THEME ON LOCAL -*/
            wp_register_style('bootstrap-theme', get_template_directory_uri() . '/css/bootstrap-theme.min.css', array('bootstrap-css'), '3.3.7', 'all');
            wp_enqueue_style('bootstrap-theme');

            /*- ANIMATE CSS ON LOCAL -*/
            wp_register_style('animate-css', get_template_directory_uri() . '/css/animate.css', false, '3.5.1', 'all');
            wp_enqueue_style('animate-css');

            /*- FONT AWESOME ON LOCAL -*/
            wp_register_style('fontawesome-css', get_template_directory_uri() . '/css/font-awesome.min.css', false, '4.6.3', 'all');
            wp_enqueue_style('fontawesome-css');

            /*- FLICKITY ON LOCAL -*/
            //wp_register_style('flickity-css', get_template_directory_uri() . '/css/flickity.min.css', false, '2.0.2', 'all');
            //wp_enqueue_style('flickity-css');

            /*- OWL ON LOCAL -*/
            wp_register_style('owl-css', get_template_directory_uri() . '/css/owl.carousel.css', false, '2.2.0', 'all');
            wp_enqueue_style('owl-css');

            /*- OWL ON LOCAL -*/
            wp_register_style('owltheme-css', get_template_directory_uri() . '/css/owl.theme.css', false, '2.2.0', 'all');
            wp_enqueue_style('owltheme-css');

            /*- OWL ON LOCAL -*/
            wp_register_style('owltrans-css', get_template_directory_uri() . '/css/owl.transitions.css', false, '2.2.0', 'all');
            wp_enqueue_style('owltrans-css');

        } else {

            /*- BOOTSTRAP CORE -*/
            wp_register_style('bootstrap-css', 'https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css', false, '3.3.7', 'all');
            wp_enqueue_style('bootstrap-css');

            /*- BOOTSTRAP THEME -*/
            wp_register_style('bootstrap-theme', 'https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap-theme.min.css', array('bootstrap-css'), '3.3.7', 'all');
            wp_enqueue_style('bootstrap-theme');

            /*- ANIMATE CSS -*/
            wp_register_style('animate-css', 'https://cdnjs.cloudflare.com/ajax/libs/animate.css/3.5.1/animate.min.css', false, '3.5.1', 'all');
            wp_enqueue_style('animate-css');

            /*- FONT AWESOME -*/
            wp_register_style('fontawesome-css', 'https://maxcdn.bootstrapcdn.com/font-awesome/4.6.3/css/font-awesome.min.css', false, '4.6.3', 'all');
            wp_enqueue_style('fontawesome-css');

            /*- FLICKITY -*/
            //wp_register_style('flickity-css', 'https://cdnjs.cloudflare.com/ajax/libs/flickity/2.0.2/flickity.min.css', false, '2.0.2', 'all');
            //wp_enqueue_style('flickity-css');

            /*- OWL -*/
            wp_register_style('owl-css', 'https://cdnjs.cloudflare.com/ajax/libs/OwlCarousel2/2.2.0/assets/owl.carousel.min.css', false, '2.2.0', 'all');
            wp_enqueue_style('owl-css');

            /*- OWL -*/
            wp_register_style('owltheme-css', 'https://cdnjs.cloudflare.com/ajax/libs/OwlCarousel2/2.2.0/assets/owl.theme.default.min.css', false, '2.2.0', 'all');
            wp_enqueue_style('owltheme-css');
        }

        /*- GOOGLE FONTS -*/
        wp_register_style('google-fonts', 'https://fonts.googleapis.com/css?family=Open+Sans:300,300italic,400,400italic,600,600italic,700,700italic,800,800italic', false, $version_remove, 'all');
        wp_enqueue_style('google-fonts');

        /*- MAIN STYLE -*/
        wp_register_style('main-style', get_template_directory_uri() . '/css/casamamita-style.min.css', false, $version_remove, 'all');
        wp_enqueue_style('main-style');

        /*- MAIN MEDIAQUERIES -*/
        wp_register_style('main-mediaqueries', get_template_directory_uri() . '/css/casamamita-mediaqueries.min.css', array('main-style'), $version_remove, 'all');
        wp_enqueue_style('main-mediaqueries');

        /*- WORDPRESS STYLE -*/
        wp_register_style('wp-initial-style', get_template_directory_uri() . '/style.css', false, $version_remove, 'all');
        wp_enqueue_style('wp-initial-style');
    }
}

add_action('init', 'casamamita_load_css');

/* --------------------------------------------------------------
    ENQUEUE AND REGISTER JS
-------------------------------------------------------------- */

if (!is_admin()) add_action("wp_enqueue_scripts", "my_jquery_enqueue", '');
function my_jquery_enqueue() {
    wp_deregister_script('jquery');
    if ($_SERVER['REMOTE_ADDR'] == '::1') {
        /*- JQUERY ON LOCAL  -*/
        wp_register_script( 'jquery', get_template_directory_uri() . '/js/jquery.min.js', false, '1.12.4', true);
    } else {
        /*- JQUERY ON WEB  -*/
        wp_register_script( 'jquery', 'https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js', false, '1.12.4', true);
    }
    wp_enqueue_script('jquery');
}


function casamamita_load_js() {
    if (!is_admin()){
        $version_remove = NULL;
        if ($_SERVER['REMOTE_ADDR'] == '::1') {
            /*- MODERNIZR ON LOCAL  -*/
            wp_register_script('modernizr', get_template_directory_uri() . '/js/modernizr.min.js', array('jquery'), '2.8.3', true);
            wp_enqueue_script('modernizr');

            /*- BOOTSTRAP ON LOCAL  -*/
            wp_register_script('bootstrap', get_template_directory_uri() . '/js/bootstrap.min.js', array('jquery'), '3.3.7', true);
            wp_enqueue_script('bootstrap');

            /*- JQUERY STICKY ON LOCAL  -*/
            wp_register_script('sticky', get_template_directory_uri() . '/js/jquery.sticky.js', array('jquery'), '1.0.4', true);
            wp_enqueue_script('sticky');

            /*- JQUERY NICESCROLL ON LOCAL  -*/
            wp_register_script('nicescroll', get_template_directory_uri() . '/js/jquery.nicescroll.min.js', array('jquery'), '3.6.8', true);
            wp_enqueue_script('nicescroll');

            /*- LETTERING  -*/
            //wp_register_script('lettering', get_template_directory_uri() . '/js/jquery.lettering.js', array('jquery'), '0.7.0', true);
            //wp_enqueue_script('lettering');

            /*- SMOOTH SCROLL -*/
            //wp_register_script('smooth-scroll', get_template_directory_uri() . '/js/smooth-scroll.js', array('jquery'), '10.0.0', true);
            //wp_enqueue_script('smooth-scroll');

            /*- IMAGESLOADED ON LOCAL  -*/
            //wp_register_script('imagesloaded', get_template_directory_uri() . '/js/imagesloaded.pkgd.js', array('jquery'), '4.1.0', true);
            //wp_enqueue_script('imagesloaded');

            /*- ISOTOPE ON LOCAL  -*/
            //wp_register_script('isotope', get_template_directory_uri() . '/js/isotope.pkgd.min.js', array('jquery'), '3.0.1', true);
            //wp_enqueue_script('isotope');

            /*- FLICKITY ON LOCAL  -*/
            //wp_register_script('flickity', get_template_directory_uri() . '/js/flickity.pkgd.min.js', array('jquery'), '2.0.2', true);
            //wp_enqueue_script('flickity');

            /*- MASONRY ON LOCAL  -*/
            //wp_register_script('masonry', get_template_directory_uri() . '/js/masonry.pkgd.min.js', array('jquery'), '4.1.0', true);
            //wp_enqueue_script('masonry');

            /*- OWL ON LOCAL -*/
            wp_register_script('owl-js', get_template_directory_uri() . '/js/owl.carousel.min.js', array('jquery'), '2.2.0', true);
            wp_enqueue_script('owl-js');

        } else {


            /*- MODERNIZR -*/
            wp_register_script('modernizr', 'https://cdnjs.cloudflare.com/ajax/libs/modernizr/2.8.3/modernizr.min.js', array('jquery'), '2.8.3', true);
            wp_enqueue_script('modernizr');

            /*- BOOTSTRAP -*/
            wp_register_script('bootstrap', 'https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js', array('jquery'), '3.3.7', true);
            wp_enqueue_script('bootstrap');

            /*- JQUERY STICKY -*/
            wp_register_script('sticky', 'https://cdnjs.cloudflare.com/ajax/libs/jquery.sticky/1.0.3/jquery.sticky.min.js', array('jquery'), '1.0.3', true);
            wp_enqueue_script('sticky');

            /*- JQUERY NICESCROLL -*/
            wp_register_script('nicescroll', 'https://cdnjs.cloudflare.com/ajax/libs/jquery.nicescroll/3.6.8-fix/jquery.nicescroll.min.js', array('jquery'), '3.6.8', true);
            wp_enqueue_script('nicescroll');

            /*- LETTERING  -*/
            //wp_register_script('lettering', 'https://cdnjs.cloudflare.com/ajax/libs/lettering.js/0.7.0/jquery.lettering.min.js', array('jquery'), '0.7.0', true);
            //wp_enqueue_script('lettering');

            /*- SMOOTH SCROLL -*/
            //wp_register_script('smooth-scroll', 'https://cdnjs.cloudflare.com/ajax/libs/smooth-scroll/10.0.0/js/smooth-scroll.min.js', array('jquery'), '10.0.0', true);
            //wp_enqueue_script('smooth-scroll');

            /*- IMAGESLOADED -*/
            //wp_register_script('imagesloaded', 'https://cdn.jsdelivr.net/imagesloaded/4.1.0/imagesloaded.pkgd.min.js', array('jquery'), '4.1.0', true);
            //wp_enqueue_script('imagesloaded');

            /*- ISOTOPE -*/
            //wp_register_script('isotope', 'https://cdnjs.cloudflare.com/ajax/libs/jquery.isotope/3.0.1/isotope.pkgd.min.js', array('jquery'), '3.0.1', true);
            //wp_enqueue_script('isotope');

            /*- FLICKITY -*/
            //wp_register_script('flickity', 'https://cdnjs.cloudflare.com/ajax/libs/flickity/2.0.2/flickity.pkgd.min.js', array('jquery'), '1.2.1', true);
            //wp_enqueue_script('flickity');

            /*- MASONRY -*/
            //wp_register_script('masonry', 'https://cdnjs.cloudflare.com/ajax/libs/masonry/4.1.0/masonry.pkgd.min.js', array('jquery'), '4.1.0', true);
            //wp_enqueue_script('masonry');

            /*- OWL ON LOCAL -*/
            wp_register_script('owl-js', 'https://cdnjs.cloudflare.com/ajax/libs/OwlCarousel2/2.2.0/owl.carousel.min.js', array('jquery'), '2.2.0', true);
            wp_enqueue_script('owl-js');

        }

        /*- MAIN FUNCTIONS -*/
        wp_register_script('main-functions', get_template_directory_uri() . '/js/functions.min.js', array('jquery'), $version_remove, true);
        wp_enqueue_script('main-functions');
    }
}

add_action('init', 'casamamita_load_js');

/* --------------------------------------------------------------
    ADD THEME SUPPORT
-------------------------------------------------------------- */

load_theme_textdomain( 'casamamita', get_template_directory() . '/languages' );
add_theme_support( 'post-formats', array( 'aside', 'gallery', 'link', 'image', 'quote', 'status', 'video', 'audio' ));
add_theme_support( 'post-thumbnails' );
add_theme_support( 'automatic-feed-links' );
add_theme_support( 'title-tag' );
add_theme_support( 'menus' );
add_theme_support( 'html5', array( 'comment-list', 'search-form', 'comment-form' ) );
add_theme_support( 'custom-background',
                  array(
                      'default-image' => '',    // background image default
                      'default-color' => '',    // background color default (dont add the #)
                      'wp-head-callback' => '_custom_background_cb',
                      'admin-head-callback' => '',
                      'admin-preview-callback' => ''
                  )
                 );

/* --------------------------------------------------------------
    ADD NAV MENUS LOCATIONS
-------------------------------------------------------------- */
function casamamita_add_editor_styles() {
    add_editor_style( get_stylesheet_directory_uri() . '/css/editor-styles.css' );
}
add_action( 'admin_init', 'casamamita_add_editor_styles' );

/* --------------------------------------------------------------
    ADD NAV MENUS LOCATIONS
-------------------------------------------------------------- */

register_nav_menus( array(
    'preheader_menu' => __( 'Menu Header Previo', 'casamamita' ),
    'header_menu' => __( 'Menu Header Principal', 'casamamita' ),
    'menu_footer' => __( 'Menu "Empresa" Footer', 'casamamita' ),
    'menu_footer_2nd' => __( 'Menu "Sobre Nosotros" Footer', 'casamamita' )
) );

/* --------------------------------------------------------------
    ADD DYNAMIC SIDEBAR SUPPORT
-------------------------------------------------------------- */

add_action( 'widgets_init', 'casamamita_widgets_init' );
function casamamita_widgets_init() {
    register_sidebars( 4, array(
        'name'          => __('Home Product %d', 'casamamita' ),
        'id'            => 'sidebar',
        'description'   => '',
        'class'         => '',
        'before_widget' => '<div id="%1$s" class="widget-promo %2$s">',
        'after_widget'  => '</div>',
        'before_title'  => '<h2 class="widgettitle">',
        'after_title'   => '</h2>' ) );

    register_sidebar( array(
        'name' => __( 'Home Product Mobile', 'casamamita' ),
        'id' => 'mobile_sidebar',
        'description' => __( 'Este Widget solo se mostrara en el Home en su edición Mobile', 'casamamita' ),
        'before_widget' => '<div id="%1$s" class="widget-promo %2$s">',
        'after_widget'  => '</div>',
        'before_title'  => '<h2 class="widgettitle">',
        'after_title'   => '</h2>'
    ) );

    register_sidebar( array(
        'name' => __( 'Main Sidebar', 'casamamita' ),
        'id' => 'main_sidebar',
        'description' => __( 'Widgets seran vistos en posts y pages', 'casamamita' ),
        'before_widget' => '<li id="%1$s" class="widget %2$s">',
        'after_widget'  => '</li>',
        'before_title'  => '<h2 class="widgettitle">',
        'after_title'   => '</h2>',
    ) );

    register_sidebar( array(
        'name' => __( 'Shop Sidebar', 'casamamita' ),
        'id' => 'shop_sidebar',
        'description' => __( 'Widgets seran vistos en Tienda y Categorias de Producto', 'casamamita' ),
        'before_widget' => '<li id="%1$s" class="widget %2$s">',
        'after_widget'  => '</li>',
        'before_title'  => '<h2 class="widgettitle">',
        'after_title'   => '</h2>',
    ) );
}



/* --------------------------------------------------------------
    CUSTOM ADMIN LOGIN
-------------------------------------------------------------- */

add_action('login_head', 'custom_login_logo');
function custom_login_logo() {
    echo '
    <style type="text/css">
        body{
            background-color: #f5f5f5 !important;
            background-image:url(' . get_template_directory_uri() . '/images/bg.jpg);
            background-repeat: no-repeat;
            background-position: center;
            background-size: cover;
        }
        h1 {
            background-image:url(' . get_template_directory_uri() . '/images/logo.png) !important;
            background-repeat: no-repeat;
            background-position: center;
            background-size: contain !important;
        }
        a { background-image:none !important; }
        .login form{
            -webkit-border-radius: 5px;
            border-radius: 5px;
            background-color: rgba(203, 203, 203, 0.5);
            box-shadow: 0px 0px 6px #878787;
        }
        .login label{
            color: black;
            font-weight: 500;
        }
    </style>
    ';
}

if (! function_exists('dashboard_footer') ){
    function dashboard_footer() {
        echo '<span id="footer-thankyou">';
        _e ('Gracias por crear con ', 'casamamita' );
        echo '<a href="http://wordpress.org/" >WordPress.</a> - ';
        _e ('Tema desarrollado por ', 'casamamita' );
        echo '<a href="http://robertochoa.com.ve/" >Robert Ochoa</a></span>';
    }
}
add_filter('admin_footer_text', 'dashboard_footer');

/* --------------------------------------------------------------
    GET CUSTOM REVSLIDER
-------------------------------------------------------------- */

function get_custom_slider() {
    global $wpdb;
    $the_query = "SELECT title, alias FROM " . $wpdb->prefix . "revslider_sliders";
    $sliders = $wpdb->get_results($the_query, 'ARRAY_A');
    $item_container = [];
    $i = 1;
    if (empty($sliders)) {
        $item_container[] = array(" " => " ");
    } else {
        foreach ($sliders as $item){
            $itemkeys[] = $item['alias'];
            $itemvalues[] = $item['title'];
        }
        $item_container[] = array_combine($itemkeys, $itemvalues);
    }
    return $item_container[0];
}

function get_custom_category() {
    $marcas = get_term_by('slug', 'marcas', 'product_cat');
    $myterms = get_terms( array( 'taxonomy' => 'product_cat', 'parent' => 0, 'hide_empty' => false, 'exclude' => array($marcas->term_id)));
    $cat_container = [];

    if ( ! empty( $terms ) && ! is_wp_error( $terms ) ){
        $cat_container[] = array(" " => " ");
    } else {
        foreach ($myterms as $item){
            $itemkeys[] = $item->slug;
            $itemvalues[] = $item->name;
        }

        for( $i=0; $i<count($itemkeys); $i++)  {
            $cat_container[$itemkeys[$i]] = $itemvalues[$i];
        }
    }

    return $cat_container;
}

/* --------------------------------------------------------------
    ADD CUSTOM METABOX
-------------------------------------------------------------- */

add_filter( 'rwmb_meta_boxes', 'casamamita_metabox' );

function casamamita_metabox( $meta_boxes )
{
    $prefix = 'rw_';

    $meta_boxes[] = array(
        'id'         => 'home_data',
        'title'      => __( 'Información Extra', 'casamamita' ),
        'post_types' => array( 'page' ),
        'context'    => 'normal',
        'priority'   => 'high',
        'include' => array(
            // With all conditions below, use this logical operator to combine them. Default is 'OR'. Case insensitive. Optional.
            'relation'        => 'OR',
            'slug'            => array( 'inicio', 'home' ),
            'template'        => array( 'templates-home.php' ),
        ),
        'fields' => array(
            array(
                'name'     => __( 'Seleccione Revolution Slider', 'casamamita' ),
                'id'       => $prefix . 'slider',
                'type'     => 'select_advanced',
                // Array of 'value' => 'Label' pairs for select box
                'options'  => get_custom_slider(),
                // Select multiple values, optional. Default is false.
                'multiple' => false,
                'std'         => ' ', // Default value, optional
                'placeholder' => __( 'Seleccione un slider', 'casamamita' ),
            ),
        )
    );

    $meta_boxes[] = array(
        'id'         => 'home_data',
        'title'      => __( 'Información Extra', 'casamamita' ),
        'post_types' => array( 'page' ),
        'context'    => 'normal',
        'priority'   => 'high',
        'include' => array(
            // With all conditions below, use this logical operator to combine them. Default is 'OR'. Case insensitive. Optional.
            'relation'        => 'OR',
            'slug'            => array( 'contacto', 'contact-us' ),
            'template'        => array( 'page-contacto.php' ),
        ),
        'fields' => array(
            array(
                'name'     => __( 'Shortcode para Formulario de Contacto', 'casamamita' ),
                'id'       => $prefix . 'contact_form',
                'type'     => 'text',
                'placeholder' => __( 'Coloque el shortcode para el Formulario', 'casamamita' ),
            ),
        )
    );

    $meta_boxes[] = array(
        'id'         => 'promo_data',
        'title'      => __( 'Datos Principales - Promociones', 'casamamita' ),
        'post_types' => array( 'promociones' ),
        'context'    => 'normal',
        'priority'   => 'high',
        'fields' => array(
            array(
                'id'   => 'custom_html',
                // Field name: usually not used
                // 'name' => __( 'Custom HTML', 'your-prefix' ),
                'type' => 'custom_html',
                // HTML content
                'std'  => '<div class="warning"><h2>Destino de Promo:</h2></div>',
                // Callback function to show custom HTML
                // 'callback' => 'display_warning',
            ),
            array(
                'name'     => __( 'Ruta de la Promo', 'casamamita' ),
                'desc'     => __( 'URL del destino que tendra la promo a mostrar', 'casamamita' ),
                'id'       => $prefix . 'promo_link',
                'type'     => 'text',
                'placeholder' => __( 'E.G: http://wwww.casamamita.com/xxxxx', 'casamamita' ),
            ),
            array(
                'id'   => 'custom_html_2',
                // Field name: usually not used
                // 'name' => __( 'Custom HTML', 'your-prefix' ),
                'type' => 'custom_html',
                // HTML content
                'std'  => '<div class="warning"><h2>Sección donde Aparecerá:</h2></div>',
                // Callback function to show custom HTML
                // 'callback' => 'display_warning',
            ),
            array(
                'name'     => __( 'Categoría de Promo', 'casamamita' ),
                'desc'     => __( 'Seleccione la Categoria donde saldra la promo en el menu <br/> y en móviles en cada sección', 'casamamita' ),
                'id'       => $prefix . 'promo_category',
                'type'     => 'select_advanced',
                // Array of 'value' => 'Label' pairs for select box
                'options'  => get_custom_category(),
                // Select multiple values, optional. Default is false.
                'multiple' => true,
                'std'         => ' ', // Default value, optional
                'placeholder' => __( 'Seleccione un slider', 'casamamita' ),
            ),
        )
    );

    return $meta_boxes;
}

/* --------------------------------------------------------------
    ADD CUSTOM POST TYPE
-------------------------------------------------------------- */

// Register Custom Post Type
function promociones() {

    $labels = array(
        'name'                  => _x( 'Promos', 'Post Type General Name', 'casamamita' ),
        'singular_name'         => _x( 'Promo', 'Post Type Singular Name', 'casamamita' ),
        'menu_name'             => __( 'Promociones', 'casamamita' ),
        'name_admin_bar'        => __( 'Promociones', 'casamamita' ),
        'archives'              => __( 'Archivo de Promos', 'casamamita' ),
        'attributes'            => __( 'Atributos de Promo', 'casamamita' ),
        'parent_item_colon'     => __( 'Promo Padre:', 'casamamita' ),
        'all_items'             => __( 'Todas las Promos', 'casamamita' ),
        'add_new_item'          => __( 'Agregar Nueva Promo', 'casamamita' ),
        'add_new'               => __( 'Agregar Nueva', 'casamamita' ),
        'new_item'              => __( 'Nueva Promo', 'casamamita' ),
        'edit_item'             => __( 'Editar Promo', 'casamamita' ),
        'update_item'           => __( 'Actualizar Promo', 'casamamita' ),
        'view_item'             => __( 'Ver Promo', 'casamamita' ),
        'view_items'            => __( 'Ver Promos', 'casamamita' ),
        'search_items'          => __( 'Buscar Promo', 'casamamita' ),
        'not_found'             => __( 'No hay resultados', 'casamamita' ),
        'not_found_in_trash'    => __( 'No hay resultados en Papelera', 'casamamita' ),
        'featured_image'        => __( 'Imagen de Promo', 'casamamita' ),
        'set_featured_image'    => __( 'Colocar Imagen de Promo', 'casamamita' ),
        'remove_featured_image' => __( 'Remover Imagen de Promo', 'casamamita' ),
        'use_featured_image'    => __( 'Usar como Imagen de Promo', 'casamamita' ),
        'insert_into_item'      => __( 'Insertar en Promo', 'casamamita' ),
        'uploaded_to_this_item' => __( 'Cargado a esta Promo', 'casamamita' ),
        'items_list'            => __( 'Listado de Promos', 'casamamita' ),
        'items_list_navigation' => __( 'Navegación del Listado de Promos', 'casamamita' ),
        'filter_items_list'     => __( 'Filtro del Listado de Promos', 'casamamita' ),
    );
    $args = array(
        'label'                 => __( 'Promo', 'casamamita' ),
        'description'           => __( 'Promociones dentro del Menu', 'casamamita' ),
        'labels'                => $labels,
        'supports'              => array( 'title', 'thumbnail', ),
        'hierarchical'          => false,
        'public'                => true,
        'show_ui'               => true,
        'show_in_menu'          => true,
        'menu_position'         => 5,
        'menu_icon'             => 'dashicons-heart',
        'show_in_admin_bar'     => true,
        'show_in_nav_menus'     => true,
        'can_export'            => true,
        'has_archive'           => true,
        'exclude_from_search'   => true,
        'publicly_queryable'    => true,
        'capability_type'       => 'post',
        'show_in_rest'          => false,
    );
    register_post_type( 'promociones', $args );

}
add_action( 'init', 'promociones', 0 );


/* --------------------------------------------------------------
    ADD CUSTOM IMAGE SIZE
-------------------------------------------------------------- */
if ( function_exists( 'add_theme_support' ) ) {
    add_theme_support( 'post-thumbnails' );
    set_post_thumbnail_size( 9999, 400, true);
}
if ( function_exists( 'add_image_size' ) ) {
    add_image_size('avatar', 100, 100, true);
    add_image_size('blog_img', 276, 217, true);
    add_image_size( 'single_img', 636, 297, true );
}


/* --------------------------------------------------------------
    ADD CUSTOM WALKER BOOTSTRAP
-------------------------------------------------------------- */

// WALKER COMPLETO TOMADO DESDE EL NAVBAR COLLAPSE
//require_once('includes/wp_bootstrap_navwalker.php');

// WALKER CUSTOM SI DEBO COLOCAR ICONOS AL LADO DEL MENU PRINCIPAL - SU ESTRUCTURA ESTA DENTRO DEL MISMO ARCHIVO
//require_once('includes/wp_walker_custom.php');


/* --------------------------------------------------------------
ADD CUSTOM WORDPRESS FUNCTIONS
-------------------------------------------------------------- */

require_once('includes/wp_custom_functions.php');

/* --------------------------------------------------------------
ADD CUSTOM WOOCOMMERCE OVERRIDES
-------------------------------------------------------------- */

require_once('includes/wp_woocommerce_functions.php');

/* SERVICE FUNCTION CALLER */
add_action( 'wp_enqueue_scripts', 'ajax_test_enqueue_scripts' );

function ajax_test_enqueue_scripts() {

    wp_enqueue_script( 'added_function', get_template_directory_uri() . '/js/add-to-cart-func.js', array('jquery'), '1.0', true );

    wp_localize_script( 'added_function', 'added_function', array(
        'ajax_url' => admin_url( 'admin-ajax.php' )
    ));

}

add_action( 'wp_ajax_nopriv_post_added_function', 'post_added_function' );
add_action( 'wp_ajax_post_added_function', 'post_added_function' );

function post_added_function() {
    global $woocommerce;
    $datos = $_POST['post_id'];
    $woocommerce->cart->add_to_cart($datos);
    die();
}

add_action( 'wp_ajax_nopriv_post_remove_function', 'post_remove_function' );
add_action( 'wp_ajax_post_remove_function', 'post_remove_function' );

function post_remove_function() {
    global $woocommerce;
    $count = $woocommerce->cart->cart_contents_count;
    if ($count == 1) {
        $woocommerce->cart->empty_cart();
    } else {
        $prod_to_remove = $_POST['post_id'];
        $prod_card_id = $_POST['post_data_id'];
        foreach ( WC()->cart->get_cart() as $cart_item_key => $cart_item ) {
            // Get the Variation or Product ID
            $prod_id = ( isset( $cart_item['variation_id'] ) && $cart_item['variation_id'] != 0 ) ? $cart_item['variation_id'] : $cart_item['product_id'];
            // Check to see if IDs match
            if( $prod_to_remove == $prod_id ) {
                // Get it's unique ID within the Cart
                WC()->cart->set_quantity($prod_card_id,0);
            }
        }
    }
    $total = esc_html__('Total: ', 'casamamita');
    $total = $total . WC()->cart->get_cart_total();
    $response = array();
    $response['id'] = $prod_to_remove;
    $response['total'] = $total;
    echo json_encode($response);

    die();
}

add_action( 'wp_ajax_nopriv_post_empty_function', 'post_empty_function' );
add_action( 'wp_ajax_post_empty_function', 'post_empty_function' );

function post_empty_function() {
    global $woocommerce;
    $woocommerce->cart->empty_cart();
    die();
}



add_action( 'wp_ajax_nopriv_post_update_function', 'post_update_function' );
add_action( 'wp_ajax_post_update_function', 'post_update_function' );

function post_update_function() {
    global $woocommerce;
    $items = $woocommerce->cart->get_cart();
    $cart_url = $woocommerce->cart->get_cart_url();
    foreach($items as $item => $values) {
        $_product = $values['data']->post; ?>

<li id="cart-product-<?php echo $values['variation_id']; ?>">
    <a href="<?php echo get_permalink($_product); ?>" title="<?php echo $_product->post_title; ?>">
        <?php $post_url = get_the_post_thumbnail_url( $_product, 'avatar' ); ?>
        <img src="<?php echo $post_url; ?>" alt="<?php echo $_product->post_title; ?>" class="img-responsive img-cart" />
        <p><?php echo $_product->post_title; ?></p>
        <?php $regular_price = get_post_meta( $values['product_id'], '_regular_price', true); ?>
        <?php if ($regular_price == ""){ ?>
        <?php $price = $values['line_total']; ?>
        <?php } else { ?>
        <?php $price = (get_post_meta($values['product_id'] , '_price', true) * $values['quantity']); ?>
        <?php } ?>
        <span>
            <?php _e('Cantidad: ', 'casamamita'); ?>
            <?php echo $values['quantity']; ?>
            <?php _e('Total: ', 'casamamita'); ?>
            <?php echo get_woocommerce_currency_symbol() . ' ' . number_format($price, 2, ',', ' '); ?>
        </span>
    </a>
    <a onclick="remove_cart('<?php echo $values['variation_id']; ?>', '<?php echo esc_attr( $item ); ?>')" class="custom-remove-product"><i class="fa fa-times"></i></a>
    <div class="clearfix"></div>
</li>
<?php } ?>
<li class="custom-cart-button">
    <div class="custom-cart-totals">
        <?php _e('Total: ', 'casamamita'); ?>
        <?php $total = $woocommerce->cart->get_cart_total(); ?>
        <?php echo $total; ?>

    </div>
    <a href="<?php echo esc_url($cart_url); ?>" class="custom-cart-link"><button class="btn btn-md btn-custom-cart"><?php _e('Ir al Carrito', 'casamamita'); ?></button></a>
</li>
<?php
    die();
}



function wp_get_menu_array($current_menu) {

    $array_menu = wp_get_nav_menu_items($current_menu);
    $menu = array();
    foreach ($array_menu as $m) {
        if (empty($m->menu_item_parent)) {
            $menu[$m->ID] = array();
            $menu[$m->ID]['ID']      =   $m->ID;
            $menu[$m->ID]['title']       =   $m->title;
            $menu[$m->ID]['url']         =   $m->url;
            $menu[$m->ID]['children']    =   array();
        }
    }
    $submenu = array();
    foreach ($array_menu as $m) {
        if ($m->menu_item_parent) {
            $submenu[$m->ID] = array();
            $submenu[$m->ID]['ID']       =   $m->ID;
            $submenu[$m->ID]['title']    =   $m->title;
            $submenu[$m->ID]['url']  =   $m->url;
            $menu[$m->menu_item_parent]['children'][$m->ID] = $submenu[$m->ID];
        }
    }
    return $menu;

}

require_once('includes/wp_menu_special.php');

?>
