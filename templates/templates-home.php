<?php
/**
 * Template Name: Home Page Template
 *
 * @package Casamamita
 * @subpackage casamamita-mk01-theme
 * @since 1.0
 */
?>
<?php get_header(); ?>
<?php the_post(); ?>
<main class="container-fluid" role="main" itemscope itemprop="mainContentOfPage" itemtype="http://schema.org/Blog">
    <div class="row">
        <section class="the-slider col-lg-12 col-md-12 col-sm-12 col-xs-12 no-paddingl no-paddingr">
            <div class="container">
                <div class="row">
                    <div class="slider-content col-lg-12 col-md-12 col-sm-12 hidden-xs">
                        <h1 class="h1-hidden"><?php echo get_bloginfo('name') . ' ' . get_bloginfo('description'); ?></h1>
                        <?php $slider = get_post_meta(get_the_ID(), 'rw_slider', true); ?>
                        <?php echo do_shortcode('[rev_slider alias="' . $slider . '"]'); ?>
                    </div>
                </div>
            </div>
            <div class="slider-content hidden-lg hidden-md hidden-sm col-xs-12 no-paddingl no-paddingr">
                <?php $slider = get_post_meta(get_the_ID(), 'rw_slider', true); ?>
                <?php echo do_shortcode('[rev_slider alias="' . $slider . '"]'); ?>
            </div>
        </section>
        <section class="the-featured-prods col-lg-12 col-md-12 col-sm-12 col-xs-12">
            <div class="container">
                <div class="row">
                    <div class="col-md-10 col-md-offset-1 col-sm-12 col-xs-12">
                        <div class="promo-item-big col-md-6 col-sm-6 col-xs-12">
                            <?php dynamic_sidebar( 'sidebar' ); ?>
                        </div>
                        <div class="col-md-6 col-sm-6 hidden-xs no-paddingl">
                            <div class="col-md-6 col-sm-6 col-xs-6">
                                <div class="promo-item-small col-md-12 col-sm-12 no-paddingl no-paddingr">
                                    <?php dynamic_sidebar( 'sidebar-2' ); ?>
                                </div>
                                <div class="promo-item-small col-md-12 col-sm-12 no-paddingl no-paddingr">
                                    <?php dynamic_sidebar( 'sidebar-3' ); ?>
                                </div>
                            </div>
                            <div class="promo-item-tall col-md-6 col-sm-6 col-xs-6 no-paddingr">
                                <?php dynamic_sidebar( 'sidebar-4' ); ?>
                            </div>

                        </div>
                    </div>
                </div>
            </div>
        </section>
        <section class="search-cat col-md-12 col-sm-12 col-xs-12 no-paddingl no-paddingr">
            <div class="container">
                <div class="row">
                    <div class="front-categories col-md-12 col-sm-12 col-xs-12">
                        <?php
                        // no default values. using these as examples
                        $taxonomies = array( 'product_cat' );
                        $args = array('menu_order' => 'asc', 'hide_empty' => false, 'parent' => 0 );  ?>
                        <?php $terms = get_terms( $taxonomies, $args ); ?>
                        <?php if ( ! empty( $terms ) && ! is_wp_error( $terms ) ){ ?>
                        <?php foreach ( $terms as $term ) { ?>
                        <?php $thumbnail_id = get_woocommerce_term_meta( $term->term_id, 'thumbnail_id', true ); ?>
                        <?php if (($term->slug != 'marcas') && ($term->slug != 'blanqueria') && ($thumbnail_id != 0)) { ?>
                        <div class="front-category-item col-md-3 col-sm-3 col-xs-4">
                            <?php $link = get_term_link($term); ?>
                            <a href="<?php echo $link; ?>">
                                <?php $thumbnail_id = get_woocommerce_term_meta( $term->term_id, 'thumbnail_id', true ); ?>
                                <?php $image = wp_get_attachment_url( $thumbnail_id ); ?>
                                <?php if ( $image ) { echo '<img src="' . $image . '" alt="" class="img-responsive"/>'; } ?>
                            </a>
                            <h5><?php echo $term->name ?></h5>
                        </div>
                        <?php } ?>
                        <?php } ?>
                        <?php } ?>
                    </div>
                </div>
            </div>
        </section>
        <section class="the-collection col-lg-12 col-md-12 col-sm-12 col-xs-12">
            <div class="container">
                <div class="row">
                    <div class="collection-container col-lg-12 col-md-12 col-sm-12 col-xs-12">
                        <h2><?php _e('NUEVA COLECCIÓN', 'casamamita'); ?></h2>
                        <div class="home-product-carousel col-lg-12 col-md-12 col-sm-12 col-xs-12 no-paddingl no-paddingr owl-carousel owl-theme">
                            <?php $args = array('post_type' => 'product', 'posts_per_page' => 10, 'order' => 'DESC', 'orderby' => 'date' ); ?>
                            <?php query_posts($args); ?>
                            <?php if ( have_posts() ) : ?>
                            <?php while (have_posts()) : the_post(); ?>
                            <?php if (has_post_thumbnail()) : ?>
                            <?php $product = new WC_Product( get_the_ID() ); ?>
                            <?php $price = $product->get_price_html(); ?>
                            <?php if ($price != '') { ?>
                            <article class="home-product-item item">

                                <div class="home-product-item-remove">
                                    <a href="<?php the_permalink(); ?>" >
                                        <img src="<?php echo esc_url(get_template_directory_uri()); ?>/images/icon.png" alt="" class="img-responsive img-cart" />
                                        <img src="<?php echo esc_url(get_template_directory_uri()); ?>/images/loader.gif" alt="" class="img-responsive img-loader" />
                                    </a>
                                </div>

                                <picture>
                                    <a href="<?php the_permalink(); ?>">
                                        <?php the_post_thumbnail('product_img', array('class' => 'img-responsive')); ?>
                                    </a>
                                </picture>
                                <header>
                                    <h3><?php the_title(); ?></h3>
                                </header>
                                <div class="home-product-item-info">
                                    <p><?php echo $product->get_price_html(); ?></p>
                                </div>
                            </article>
                            <?php } ?>
                            <?php endif; ?>
                            <?php endwhile; ?>
                            <?php endif; ?>
                            <?php wp_reset_query(); ?>
                        </div>

                    </div>
                    <div class="home-mobile-widget col-xs-12 hidden-lg hidden-md hidden-sm no-paddingl no-paddingr">
                        <?php dynamic_sidebar( 'mobile_sidebar' ); ?>
                    </div>
                </div>
            </div>
        </section>

        <section class="the-brands col-lg-12 col-md-12 col-sm-12 hidden-xs">
            <h2><?php _e('NUESTRAS MARCAS', 'casamamita'); ?></h2>
            <div class="container">
                <div class="row">
                    <div class="home-partners-content col-lg-12 col-md-12 col-sm-12 no-paddingl no-paddingr">
                        <div class="home-brands-carousel col-lg-12 col-md-12 col-sm-12 col-xs-12 no-paddingl no-paddingr owl-carousel owl-theme">
                            <?php $marcas = get_term_by( 'slug', 'marcas', 'product_cat' ); ?>
                            <?php $args = array('taxonomy' => 'product_cat', 'order' => 'ASC', 'orderby' => 'date', 'hide_empty' => false, 'parent' => $marcas->term_id ); ?>
                            <?php $marcas_terms = get_terms($args); ?>
                            <?php foreach ($marcas_terms as $itemterm) { ?>
                            <article class="home-brands-item item">
                                <picture>
                                    <?php $thumbnail_id = get_woocommerce_term_meta( $itemterm->term_id, 'thumbnail_id', true ); ?>
                                    <?php $image = wp_get_attachment_url( $thumbnail_id ); ?>

                                    <a href="<?php echo get_term_link($itemterm); ?>" title="Productos <?php echo $itemterm->name; ?>">
                                        <?php echo "<img src='{$image}' alt='Productos {$itemterm->name}' class='img-responsive' />"; ?>
                                    </a>
                                </picture>
                            </article>
                            <?php } ?>
                        </div>
                    </div>
                </div>
            </div>
        </section>
        <section class="the-instagram col-lg-12 col-md-12 col-sm-12 col-xs-12 no-paddingl no-paddingr">
            <div class="container">
                <div class="row">
                    <div class="instagram-container col-lg-12 col-md-12 col-sm-12 hidden-xs no-paddingl no-paddingr">
                        <h2><?php _e('INSTAGRAM', 'casamamita'); ?></h2>
                        <div class="instagram-info col-md-3 col-sm-3 hidden-xs">
                            <div class="instagram-info-border">
                                <h3><?php _e('Chequea', 'casamamita'); ?></h3>
                                <img src="<?php echo esc_url(get_template_directory_uri()); ?>/images/instagram.png" alt="Instagram" class="img-responsive" />
                                <h4><a href="https://www.instagram.com/casamamita">@casamamita</a></h4>
                            </div>
                        </div>
                        <div class="instagram-renderer col-md-9 col-sm-9 col-xs-12 no-paddingl">
                            <?php echo do_shortcode('[instagram-feed]'); ?>
                        </div>
                    </div>
                </div>
            </div>
            <div class="instagram-container hidden-lg hidden-md hidden-sm col-xs-12 no-paddingl no-paddingr">
                <h2><?php _e('INSTAGRAM', 'casamamita'); ?></h2>
                <div class="instagram-renderer col-xs-12 no-paddingl no-paddingr">
                    <?php echo do_shortcode('[instagram-feed num=6 cols=4]'); ?>
                </div>
            </div>
        </section>

    </div>
</main>
<?php get_footer(); ?>
