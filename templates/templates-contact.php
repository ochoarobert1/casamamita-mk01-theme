<?php
/**
 * Template Name: Contact Page
 *
 * @package Casamamita
 * @subpackage casamamita-mk01-theme
 * @since 1.0
 */
?>

<?php get_header(); ?>
<?php the_post(); ?>
<main class="container" role="main" itemscope itemprop="mainContentOfPage" itemtype="http://schema.org/Blog">
    <div class="row">
        <section class="page-container col-md-12" role="article" itemscope itemtype="http://schema.org/BlogPosting">
            <div class="the-breadcrumbs col-md-12">
                <?php echo the_breadcrumb(); ?>
            </div>
            <h1 itemprop="headline"><?php the_title(); ?></h1>
            <section class="page-container col-lg-12 col-md-12 col-sm-12 col-xs-12" role="article" itemscope itemtype="http://schema.org/BlogPosting">
                <div class="container">
                    <div class="row">
                        <article id="post-<?php the_ID(); ?>" class="page-content <?php echo join(' ', get_post_class()); ?>" >
                            <div class="page-article page-contact-content col-lg-4 col-md-4 col-sm-4 col-xs-12" itemprop="articleBody">
                                <?php the_content(); ?>
                            </div>
                            <picture class="page-contact-form col-lg-6 col-lg-offset-1 col-md-6 col-md-offset-1 col-sm-6 col-sm-offset-1 col-xs-12 no-paddingl no-paddingr">
                                <?php $contact_form = get_post_meta(get_the_ID(), 'rw_contact_form', true); ?>
                                <?php echo do_shortcode($contact_form); ?>
                            </picture>
                        </article>
                    </div>
                </div>
            </section>
        </section>
    </div>
</main>
<?php get_footer(); ?>
